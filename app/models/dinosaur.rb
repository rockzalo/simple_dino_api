class Dinosaur < ApplicationRecord
  belongs_to :dinosaur_type

  validates :name, presence: :true, uniqueness: true
end
