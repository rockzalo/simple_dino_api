class DinosaurType < ApplicationRecord
  has_many :dinosaurs

  validates :name, presence: :true

  def to_s
    name
  end
end
