class DinosaurSerializer
  include FastJsonapi::ObjectSerializer
  cache_options enabled: true, cache_length: 1.week
  attributes :name, :description, :name_pronunciation, :name_meaning, :image_url, :length, :weight, :diet, :when_it_lived, :found_in

  attribute :type do |dino|
    dino.dinosaur_type.to_s
  end
end
