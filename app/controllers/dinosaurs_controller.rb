class DinosaursController < ApplicationController
  before_action :set_dinosaur, only: :show

  def ping
    expires_in 6.hours, public: true

    render plain: "pong"
  end

  # GET /dinosaurs
  def index
    expires_in 6.hours, public: true

    @q = Dinosaur.order(:name).ransack(params[:q])
    @dinosaurs = DinosaurSerializer.new(@q.result.includes(:dinosaur_type)).serialized_json

    render json: @dinosaurs
  end

  # GET /dinosaurs/1
  def show
    expires_in 6.hours, public: true
    
    render json: @dinosaur
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dinosaur
      @dinosaur = Dinosaur.find(params[:id])
    end
end
