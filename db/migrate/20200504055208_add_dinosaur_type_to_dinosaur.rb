class AddDinosaurTypeToDinosaur < ActiveRecord::Migration[6.0]
  def change
    add_reference :dinosaurs, :dinosaur_type, null: false, foreign_key: true
  end
end
