class CreateDinosaurs < ActiveRecord::Migration[6.0]
  def change
    create_table :dinosaurs do |t|
      t.string :name
      t.text :description
      t.string :name_pronunciation
      t.string :name_meaning
      t.string :image_url
      t.string :length
      t.string :weight
      t.string :diet
      t.string :when_it_lived
      t.string :found_in

      t.timestamps
    end
  end
end
