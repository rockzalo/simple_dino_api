require 'csv'

table = CSV.parse(File.read(Rails.root.join("db/seeds/dinos.csv")), headers: true)

table.each do |row|
  type = DinosaurType.find_or_create_by(name: row["type"].strip.titleize)
  Dinosaur.create(
    name: row["name"].strip,
    description: row["description"],
    name_pronunciation: row["pronunciation"],
    name_meaning: row["meaning"].gsub("'", ""),
    image_url: row["image"],
    length: row["length"],
    weight: row["weight"],
    diet: row["diet"],
    when_it_lived: row["when"],
    found_in: row["found"],
    dinosaur_type: type
  )
end