Rails.application.routes.draw do
  root to: 'dinosaurs#ping' 
  resources :dinosaurs, only: [:index, :show]
end
